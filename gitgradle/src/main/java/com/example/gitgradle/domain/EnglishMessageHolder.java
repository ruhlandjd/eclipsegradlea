package com.example.gitgradle.domain;

public class EnglishMessageHolder implements MessageHolder {

	@Override
	public String getMessage() {
		return "Greetings, sir or madam!";
	}
}
