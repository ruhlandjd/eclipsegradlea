package com.example.gitgradle.domain;

public interface MessageHolder {
	String getMessage();
}
