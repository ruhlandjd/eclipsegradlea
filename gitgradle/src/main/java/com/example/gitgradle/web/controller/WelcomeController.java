package com.example.gitgradle.web.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WelcomeController {
	
	@RequestMapping(path = "/hello")
	public Object welcome() {
		return "Hi!";		// <<- This is not JSON!!!  Let's fix it!
	}	
	
}
